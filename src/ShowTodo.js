import React from 'react';

const ShowTodo = ({todos, removeTodo}) => {

    const todoLists = todos.map(todo => {
        return(
            <div className="todo-item" key={todo.id}>
                <p>{todo.desc}</p>
                <button 
                    className="remove-item"
                    onClick={() => {removeTodo(todo.id)}}>
                    <span>❌</span> 
                </button>
            </div>
        )
    })

    return (
        <div className="todo-lists">
            {todoLists}
        </div>
    )
}

export default ShowTodo;