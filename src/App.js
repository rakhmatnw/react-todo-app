import React, { Component } from 'react';
import ShowTodo from './ShowTodo';
import AddTodo from './AddTodo';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    todos: []
  }

  addTodo = (todo) => {
    todo.id = this.state.todos.length + 1;
    const todos = [...this.state.todos, todo];

    this.setState({
      todos: todos
    });
  }

  removeTodo = (id) => {
    console.log(id);
    const todos = this.state.todos.filter(todo => {
      return todo.id !== id
    });

    this.setState({
      todos: todos
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React To Do</h1>
          <AddTodo addTodo={this.addTodo}/>
          <ShowTodo 
            todos={ this.state.todos }  
            removeTodo={this.removeTodo}/>
        </header>
      </div>
    );
  }
}

export default App;
