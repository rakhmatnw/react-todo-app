import React, { Component } from 'react';

class AddTodo extends Component {

    state = {
        desc: null
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addTodo(this.state);
        this.setState({
            desc: ''
        })
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    render() {
        return(
            <form 
                className="todo-form"
                onSubmit={this.handleSubmit}>
                <input 
                    type="text" 
                    id="desc"
                    className="todo-input"
                    placeholder="insert what to do.."
                    onChange={this.handleChange}
                    value={this.state.desc}/>
            </form>
        )
    }
}

export default AddTodo;